/************************************************************************
 * @file       twilight switch.c
 * @autor      rth.
 * @date       27 Mar 2018
 * @brief      twilight switch main file
 *
 * @license    GNU GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 *
 ************************************************************************/

#define ONOFF   0
#define PWM     1
#define TEMP    0

#define useDefCpuFreq 0   //   enable default frequency for setting fuses
                          //  !!!      at lowest frequency enable/disable of       !!!
                          //  !!! debugWire is not functional, set back to default !!!

#define WDGENA  1   // use watchdog


#ifndef F_CPU                       // only for '_delay_ms()'
	#if useDefCpuFreq	            // ATtiny85 internal oscillator (8MHz)
		#define F_CPU 1000000UL     // SysClk set to 1MHz by prescaler
	#else
		#define F_CPU   31250UL     // SysClk set to 31.25kHz by prescaler
	#endif
#endif


#include "src/system.h"
#include "src/led.h"

#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

uint8_t cnt=0x00;
uint8_t tog=0x00;


ISR(TIMER0_OVF_vect) {
    static uint8_t volatile cnt=0;
    cli();
    if(++cnt>=0x1F) {
        if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
        else PORTB |= (1<<PB2);
        cnt=0;
    }        
    sei();
}    

ISR(TIMER1_COMPA_vect) {
    cli();
    // toggle pin led PB2
    if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
    else PORTB |= (1<<PB2);
    sei();
}

ISR(TIMER1_COMPB_vect) {
    cli();
    // toggle pin led PB2
    if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
    else PORTB |= (1<<PB2);
    sei();
}


ISR(TIMER1_OVF_vect) {
    cli();
    
    // toggle pin led PB2
    if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
    else PORTB |= (1<<PB2);

#if 1
    // update timer1 frequency
    // 256 samples until overflow 0x01=122Hz  (at 31,25kHz SysCkl)
    uint8_t tmp = TCCR1 & 0x0F;
    if(tmp == 0x01) tmp = 0x02;         //  60   Hz
    else if(tmp == 0x02) tmp = 0x03;    //  30   Hz
    else if(tmp == 0x03) tmp = 0x04;    //  15   Hz
    else if(tmp == 0x04) tmp = 0x05;    //   7,5 Hz
    else if(tmp == 0x05) tmp = 0x06;    //   3,7 Hz
    else if(tmp == 0x06) tmp = 0x01;    // 120 Hz
    TCCR1 = (TCCR1 & 0xF0) | tmp;
#else     
    TCCR1 = (TCCR1 & 0xF0) | 0x06;  
#endif
    sei();
}


ISR(ADC_vect) { 
    cli();
    if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
    else PORTB |= (1<<PB2);
    //_delay_ms(50);
    sei();
}

ISR(WDT_vect) {
    cli();
    if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
    else PORTB |= (1<<PB2);
    //_delay_ms(50);
    sei();
}

/*____________________________________________________________________________*/
int main(void) {
	
#if useDefCpuFreq
    init(f1M, WDGENA);
#else
    init(f31k25, WDGENA);
#endif
    OCR0A = 0xFF;
    OCR0B = 0x00;

    while (1) {
        
        cnt++;

#if ONOFF
        ledSwitch(LED1, true);
        ledSwitch(LED2, false);
        _delay_ms(1500);
        ledSwitch(LED1, false);
        ledSwitch(LED2, true);
        _delay_ms(1000);
#elif PWM

        //_delay_ms(0);
        
        if(OCR0A==0xFF || OCR0A==0x00) tog = ~tog;
        
        if(tog) {
            OCR0A++;
            OCR0B--;
        } else {
            OCR0B++;
            OCR0B--;
        }            
        
        if(OCR0A==0x00 || OCR0A==0xFF) {
            _delay_ms(1000);
        }            

#elif TEMP
        
        ADCSRA |= (1<<ADSC);
        while(!(ADCSRA & (1<<ADIF))){};
        if(ADCSRA & (1<<ADIF)) ADCSRA &= ~(1<<ADIF);

        int16_t volatile val16 = ADC>>2;
        //uint8_t volatile  val8 = ADCH;
        OCR0A = 0xFF - (val16 & 0x00FF);
        OCR0B = 0xFF - (val16 & 0x00FF);
        
#else        
        asm volatile ("nop");
        _delay_ms(250);
        if( PINB & (1<<PINB2) ) PORTB &= ~(1<<PB2);
        else PORTB |= (1<<PB2);
#endif

			
    }
}

