/************************************************************************
 * @file       system.c
 * @autor      rth.
 * @date       27 Mar 2018
 * @brief      system configuration
 *
 * @license    GNU GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 *
 ************************************************************************/

#include "system.h"
#include "led.h"

#include <avr/interrupt.h>


/*____________________________________________________________________________*/
void init(enum cpuFreq f, bool wdgEna) {

    //////////////////////////////////////////////////////////////////////////
    //                        *** ATtiny85 pinout [DIP] ***
    //                                  _____
    //          reset {  RST/dW|PB5  -1|�    |8-  VCC
    //      analog_in {    ACD3|PB3  -2|     |7-  PB2|DI    } digital in
    //      analog_in {    ADC2|PB4  -3|     |6-  PB1|OC0B  } PWM_out
    //                          GND  -4|_____|5-  PB0|OC0A  } PWM_out
    //////////////////////////////////////////////////////////////////////////
    
    
    //////////////////////////////////////////////////////////////////////////
    // *** MCU Control Registert & Power Reductio Register ***
    //  SM1 | SM0 | Sleep Mode --> SE: Sleep Enable
    // -----+-----+-------------------------------------
    //   0  |  0  | Idle
    //   0  |  1  | ADC Noise Reduction
    //   1  |  0  | Power-down
    //   1  |  1  | Reserved
    //
    //  ISC01 | ISC00 | Description --> INT0 Sense Control
    // -------+-------+-------------------------------------------------------------
    //    0   |   0   | The low level of INT0 generates an interrupt request.
    //    0   |   1   | Any logical change on INT0 generates an interrupt request.
    //    1   |   0   | The falling edge of INT0 generates an interrupt request.
    //    1   |   1   | The rising edge of INT0 generates an interrupt request.
    //
    //  [MCUCR]| PUD   | Pull-up Disable
    //         | BODS  | BOD Sleep
    //         | BODSE | BOD Sleep Enable
    //  [PRR]  | PRTIM1| Power Reduction Timer/Counter1
    //         | PRTIM0| Power Reduction Timer/Counter0
    //         | PRUSI | Power Reduction USI
    //         | PRADC | Power Reduction ADC
    //////////////////////////////////////////////////////////////////////////
    MCUSR = (0<<WDRF)|(0<<BORF)|(0<<EXTRF)|(0<<PORF);   // MCU status register
    MCUCR = (0<<BODS)|(0<<PUD)|(0<<SE)|(1<<SM1)|(0<<SM0)|(0<<BODSE)|(1<<ISC01)|(1<<ISC00);
    PRR = (0<<PRTIM1)|(0<<PRTIM0)|(1<<PRUSI)|(0<<PRADC); 


    //////////////////////////////////////////////////////////////////////////
    // *** SYSTEM CLOCK ***
    setClockPrescaler(f);            // configure clock prescaler
    
    
    //////////////////////////////////////////////////////////////////////////
    //  *** TIMER0 -> PWM ***
    //  COM0A1 | COM0A0 |
    //  COM0B1 | COM0B0 |  Description
    // --------+--------+-----------------------------------------------------
    //    0    |   0    |  Normal port operation, OC0A/OC0B disconnected
    //    0    |   1    |  Reserved
    //    1    |   0    |  Clear OC0A/OC0B on Compare Match when up-counting
    //         |        |  Set OC0A/OC0B on Compare Match when down-counting
    //    1    |   1    |  Set OC0A/OC0B on Compare Match when up-counting
    //         |        |  Clear OC0A/OC0B on Compare Match when down-counting
    //
    //  Mode | WGM02 | WGM01 | WGM00 | OpMode             | TOP  | Update of OCRx at | TOV Flag Set on
    // ------+-------+-------+-------+--------------------+------+-------------------+-----------------
    //   0   |   0   |   0   |   0   | Normal             | 0xFF | Immediate         | MAX
    //   1   |   0   |   0   |   1   | PWM, Phase Correct | 0xFF | TOP               | BOTTOM
    //   2   |   0   |   1   |   0   | CTC                | OCRA | Immediate         | MAX
    //   3   |   0   |   1   |   1   | Fast PWM           | 0xFF | BOTTOM            | MAX
    //   4   |   1   |   0   |   0   | Reserved           |  �   |   �               |  �
    //   5   |   1   |   0   |   1   | PWM, Phase Correct | OCRA | TOP               | BOTTOM
    //   6   |   1   |   1   |   0   | Reserved           |  �   |   �               |  �
    //   7   |   1   |   1   |   1   | Fast PWM           | OCRA | BOTTOM            | TOP
    //
    //  CS02 | CS01 | CS00 | Description
    // ------+------+------+-----------------------------------------------------------
    //    0  |   0  |   0  | No clock source (Timer/Counter stopped)
    //    0  |   0  |   1  | clkI/O  / (No prescaling)
    //    0  |   1  |   0  | clkI/O  /8 (From prescaler)
    //    0  |   1  |   1  | clkI/O  /64 (From prescaler)
    //    1  |   0  |   0  | clkI/O  /256 (From prescaler)
    //    1  |   0  |   1  | clkI/O  /1024 (From prescaler)
    //    1  |   1  |   0  | External clock source on T0 pin. Clock on falling edge
    //    1  |   1  |   1  | External clock source on T0 pin. Clock on rising edge 
    //////////////////////////////////////////////////////////////////////////
    TCCR0A = (1<<COM0A1)|(0<<COM0A0)|(1<<COM0B1)|(0<<COM0B0)|(1<<WGM01)|(1<<WGM00);
    TCCR0B = (0<<WGM02)|(0<<CS02)|(0<<CS01)|(1<<CS00);
   

    //////////////////////////////////////////////////////////////////////////
    //  *** TIMER1 -> SYSTEM TICKS (msec) ***
    //////////////////////////////////////////////////////////////////////////
    //  Timer/Counter1 Clock Prescale Select in the Asynchronous Mode
    //   PWM   | Clock  | CS1[3:0] | OCR1C | RESOLUTION
    // --------+--------+----------+-------+----------------
    //  20 kHz | PCK/16 |   0101   |  199  | 7.6
    //  30 kHz | PCK/16 |   0101   |  132  | 7.1
    //  40 kHz | PCK/8  |   0100   |  199  | 7.6
    //  50 kHz | PCK/8  |   0100   |  159  | 7.3
    //  60 kHz | PCK/8  |   0100   |  132  | 7.1
    //  70 kHz | PCK/4  |   0011   |  228  | 7.8
    //  80 kHz | PCK/4  |   0011   |  199  | 7.6
    //  90 kHz | PCK/4  |   0011   |  177  | 7.5
    // 100 kHz | PCK/4  |   0011   |  159  | 7.3
    // 110 kHz | PCK/4  |   0011   |  144  | 7.2
    // 120 kHz | PCK/4  |   0011   |  132  | 7.1
    // 130 kHz | PCK/2  |   0010   |  245  | 7.9
    // 140 kHz | PCK/2  |   0010   |  228  | 7.8
    // 150 kHz | PCK/2  |   0010   |  212  | 7.7
    // 160 kHz | PCK/2  |   0010   |  199  | 7.6
    // 170 kHz | PCK/2  |   0010   |  187  | 7.6
    // 180 kHz | PCK/2  |   0010   |  177  | 7.5
    // 190 kHz | PCK/2  |   0010   |  167  | 7.4
    // 200 kHz | PCK/2  |   0010   |  159  | 7.3
    // 250 kHz | PCK    |   0001   |  255  | 8.0
    // 300 kHz | PCK    |   0001   |  212  | 7.7
    // 350 kHz | PCK    |   0001   |  182  | 7.5
    // 400 kHz | PCK    |   0001   |  159  | 7.3
    // 450 kHz | PCK    |   0001   |  141  | 7.1
    // 500 kHz | PCK    |   0001   |  127  | 7.0
    // 
    //  CS13 | CS12 | CS11 | CS10 | Asynchronous  | Synchronous
    //    0  |   0  |   0  |   0  |  T/C1 stopped |  T/C1 stopped
    //    0  |   0  |   0  |   1  |  PCK          |  CK
    //    0  |   0  |   1  |   0  |  PCK/2        |  CK/2
    //    0  |   0  |   1  |   1  |  PCK/4        |  CK/4
    //    0  |   1  |   0  |   0  |  PCK/8        |  CK/8
    //    0  |   1  |   0  |   1  |  PCK/16       |  CK/16
    //    0  |   1  |   1  |   0  |  PCK/32       |  CK/32
    //
    //  COM1A1 | COM1A0 | Description
    // --------+--------+----------------------------------------------------------------
    //    0    |   0    | Timer/Counter Comparator A disconnected from output pin OC1A.
    //    0    |   1    | Toggle the OC1A output line.
    //    1    |   0    | Clear the OC1A output line.
    //    1    |   1    | Set the OC1A output line
    //////////////////////////////////////////////////////////////////////////
    TCCR1 = (1<<CTC1)|(0<<PWM1A)|(0<<COM1A1)|(0<<COM1A0)|(1<<CS13)|(0<<CS12)|(0<<CS11)|(0<<CS10);
    OCR1A = 0x00;
    OCR1B = 0xE0;
    OCR1C = 0xF0; // 0x96 = 5msec, 0x3E = 2ms, 0x20 = 1msec
    GTCCR = (0<<PWM1B)|(0<<COM1B1)|(0<<COM0B0)|(0<<FOC1B)|(0<<FOC1A)|(0<<PSR1);
    PLLCSR = (0<<LSM)|(0<<PCKE)|(0<<PLLE);


    //////////////////////////////////////////////////////////////////////////
    // *** ADC MULTIPLEXER & TEMPERATURE SENSOR ***
    //
    //  REFS2 | REFS1 | REFS0 | Voltage Reference (VREF) Selection
    // -------+-------+-------+---------------------------------------------------------------
    //   X    |  0    |  0    | VCC used as Voltage Reference, disconnected from PB0 (AREF).
    //   X    |  0    |  1    | External Voltage Reference at PB0 (AREF) pin, Internal Voltage
    //        |       |       | Reference turned off.
    //   0    |  1    |  0    | Internal 1.1V Voltage Reference.
    //   0    |  1    |  1    | Reserved
    //   1    |  1    |  0    | Internal 2.56V Voltage Reference without external bypass
    //        |       |       | capacitor, disconnected from PB0 (AREF).
    //   1    |  1    |  1    | Internal 2.56V Voltage Reference with external bypass capacitor
    //        |       |       | at PB0 (AREF) pin.
    //
    //           | Single Ended |  Pos. Diff. |  Neg. Diff. | 
    //  MUX[3:0] |   Input      |    Input    |    Input    | Gain
    // ----------+--------------+-------------+-------------+--------------
    //   0000    | ADC0 (PB5)   |             
    //   0001    | ADC1 (PB2)   |              
    //   0010    | ADC2 (PB4)   |                N/A
    //   0011    | ADC3 (PB3)   |
    // ----------+--------------+-------------+-------------+--------------
    //   0100    |              |  ADC2 (PB4) |  ADC2 (PB4) |  1x
    //   0101    |              |  ADC2 (PB4) |  ADC2 (PB4) | 20x
    //   0110    |              |  ADC2 (PB4) |  ADC3 (PB3) |  1x
    //   0111    |              |  ADC2 (PB4) |  ADC3 (PB3) | 20x
    //   1000    |    N/A       |  ADC0 (PB5) |  ADC0 (PB5) |  1x
    //   1001    |              |  ADC0 (PB5) |  ADC0 (PB5) | 20x
    //   1010    |              |  ADC0 (PB5) |  ADC1 (PB2) |  1x
    //   1011    |              |  ADC0 (PB5) |  ADC1 (PB2) | 20x
    // ----------+--------------+-------------+-------------+--------------
    //   1100    | VBG          |
    //   1101    | GND          |                N/A
    //   1110    | N/A          |
    //   1111    | ADC4         |  <-- Temperature Sensor
    //
    //  ADPS2 | ADPS1 | ADPS0 | Division Factor (Prescaler)
    // -------+-------+-------+-------------------------------
    //    0   |   0   |   0   |     2
    //    0   |   0   |   1   |     2
    //    0   |   1   |   0   |     4
    //    0   |   1   |   1   |     8
    //    1   |   0   |   0   |    16
    //    1   |   0   |   1   |    32
    //    1   |   1   |   0   |    64
    //    1   |   1   |   1   |   128
    //
    //  ADLAR: ADC Left Adjust Result
    //
    //  ADTS2 | ADTS1 | ADTS0 | Trigger Source
    // -------+-------+-------+-------------------------------------
    //    0   |   0   |   0   | Free Running mode
    //    0   |   0   |   1   | Analog Comparator
    //    0   |   1   |   0   | External Interrupt Request 0
    //    0   |   1   |   1   | Timer/Counter0 Compare Match A
    //    1   |   0   |   0   | Timer/Counter0 Overflow
    //    1   |   0   |   1   | Timer/Counter0 Compare Match B
    //    1   |   1   |   0   | Pin Change Interrupt Request
    //
    //  ACIS1 | ACIS0 | Interrupt Mode
    // -------+-------+--------------------------------------------
    //    0   |   0   | Comparator Interrupt on Output Toggle.
    //    0   |   1   | Reserved
    //    1   |   0   | Comparator Interrupt on Falling Output Edge.
    //    1   |   1   | Comparator Interrupt on Rising Output Edge.
    //////////////////////////////////////////////////////////////////////////
    ADMUX = (1<<REFS1)|(0<<REFS0)|(0<<ADLAR)|(0<<REFS2)|(0<<MUX3)|(0<<MUX2)|(1<<MUX1)|(1<<MUX0);
    ADCSRA = (1<<ADEN)|(0<<ADSC)|(1<<ADATE)|(0<<ADIE)|(0<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
    ADCSRB = (0<<BIN)|(0<<ACME)|(0<<IPR)|(0<<ADTS2)|(0<<ADTS1)|(0<<ADTS0);
    ACSR = (0<<ACD)|(1<<ACBG)|(0<ACIE)|(0<<ACIS1)|(0<<ACIS0);
    DIDR0 = (1<<AIN1D)|(1<<AIN0D);

    //////////////////////////////////////////////////////////////////////////
    // *** WATCHDOG ***
    //  WDP3 | WDP2 | WDP1 | WDP0 | WDT Oscil. Cycles | Typ.Time-out (5.0V)
    // ------+------+------+------+-------------------+---------------------------
    //   0   |  0   |  0   |  0   |    2K (2048)      |  16 ms
    //   0   |  0   |  0   |  1   |    4K (4096)      |  32 ms
    //   0   |  0   |  1   |  0   |    8K (8192)      |  64 ms
    //   0   |  0   |  1   |  1   |   16K (16384)     |   0.125 s
    //   0   |  1   |  0   |  0   |   32K (32764)     |   0.25 s
    //   0   |  1   |  0   |  1   |   64K (65536)     |   0.5 s
    //   0   |  1   |  1   |  0   |  128K (131072)    |   1.0 s
    //   0   |  1   |  1   |  1   |  256K (262144)    |   2.0 s
    //   1   |  0   |  0   |  0   |  512K (524288)    |   4.0 s
    //   1   |  0   |  0   |  1   | 1024K (1048576)   |   8.0 s
    //  ...  | ...  | ...  | ...  |              
    //   1   |  1   |  1   |  1   |             Reserved
    //////////////////////////////////////////////////////////////////////////
    WDTCR = (0<<WDIF)|(0<<WDIE)|(1<<WDP3)|(0<<WDCE)|(0<<WDE)|(0<<WDP2)|(0<<WDP1)|(1<<WDP0);    // watchdog timer control register

    
    //////////////////////////////////////////////////////////////////////////
    // *** INTERRUPT ***
    //  Vector | Prg.Addr | Source       | Interrupt Definition
    // --------+----------+--------------+---------------------------------
    //     1   |  0x0000  | RESET        | External Pin, Power-on Reset,
    //         |          |              | Brown-out Reset, Watchdog Reset
    //     2   |  0x0001  | INT0         | External Interrupt Request 0
    //     3   |  0x0002  | PCINT0       | Pin Change Interrupt Request 0
    //     4   |  0x0003  | TIMER1_COMPA | Timer/Counter1 Compare Match A
    //     5   |  0x0004  | TIMER1_OVF   | Timer/Counter1 Overflow
    //     6   |  0x0005  | TIMER0_OVF   | Timer/Counter0 Overflow
    //     7   |  0x0006  | EE_RDY       | EEPROM Ready
    //     8   |  0x0007  | ANA_COMP     | Analog Comparator
    //     9   |  0x0008  | ADC          | ADC Conversion Complete
    //    10   |  0x0009  | TIMER1_COMPB | Timer/Counter1 Compare Match B
    //    11   |  0x000A  | TIMER0_COMPA | Timer/Counter0 Compare Match A
    //    12   |  0x000B  | TIMER0_COMPB | Timer/Counter0 Compare Match B
    //    13   |  0x000C  | WDT          | Watchdog Time-out256/8,192
    //    14   |  0x000D  | USI_START    | USI START
    //    15   |  0x000E  | USI_OVF      | USI Overflow
    //////////////////////////////////////////////////////////////////////////
    TIMSK = (1<<OCIE1A)|(1<<OCIE1B)|(0<<OCIE0A)|(0<<OCIE0B)|(0<<OCIE0B)|(0<<TOIE1)|(0<<TOIE0);


    //////////////////////////////////////////////////////////////////////////
    // *** I/O -> PORTB ***
    //  DDxn | PORTxn | PUD |  I/O   | Pull-up | Comment
    // ------+--------+-----+--------+---------+------------------------------------------------
    //   0   |   0    |  X  | Input  |   No    | Tri-state (Hi-Z)
    //   0   |   1    |  0  | Input  |   Yes   | Pxn will source current if ext. pulled low.
    //   0   |   1    |  1  | Input  |   No    | Tri-state (Hi-Z)
    //   1   |   0    |  X  | Output |   No    | Output Low (Sink)
    //   1   |   1    |  X  | Output |   No    | Output High (Source)
    //////////////////////////////////////////////////////////////////////////
    DDRB = (0<<DDB5)|(0<<DDB4)|(0<<DDB3)|(1<<DDB2)|(1<<DDB1)|(1<<DDB0);
    PORTB = (0<<PB5)| (0<<PB4)| (0<<PB3)| (1<<PB2)| (0<<PB1)| (0<<PB0);




//POWER SAVING??????
        //set_sleep_mode(SLEEP_MODE_PWR_DOWN);
        //sleep_mode();       // in den Schlafmodus wechseln
        
        //MCUCR = (1<<SM1)|(0<<SM0);
        //MCUCR = (1<<SE);
        //asm volatile ("nop");
        //asm volatile ("sleep");
        
        sei();  // global interrupt enable
}


/*____________________________________________________________________________*/
void setClockPrescaler(enum cpuFreq f) {
    
    //////////////////////////////////////////////////////////////////////////
    //  *** SELECT CLOCK SOURCE VIA FUSE BITS ***
    //  Clock Source / '1' means unprogrammed while '0' means programmed:
    //  CKSEL[3:0] = 0000       external clock
    //             = 0001       high frequency PLL clock
    //   default-> = 0010       calibrated internal oscillator
    //             = 0011       calibrated internal oscillator (ATtiny15 comp.)
    //             = 0100       internal 128 kHz  oscillator
    //             = 0110       low-frequency crystal oscillator
    //             = 1000-1111  crystal oscillator / ceramic resonator
    //             = 0101,0111  reserved
    //
    //  Start-up times for internal oscillator
    //  SUT[1:0] = 00  6CK   14CK          BOD enabled
    //           = 01  6CK   14CK +  4ms   fast rising power
    //  default->= 10  6CK   14CK + 64ms   slowly rising power
    //           = 11  reserved
    //////////////////////////////////////////////////////////////////////////
    //        MACRO DEFINITION    |    FREQ    | CLKPS[3:0] | CLK DIV
    //   -------------------------+------------+------------+-----------
    //    #define F_CPU 8000000UL |   8.0  MHz |    0000    |    1
    //    #define F_CPU 4000000UL |   4.0  MHz |    0001    |    2
    //    #define F_CPU 2000000UL |   2.0  MHz |    0010    |    4
    //    #define F_CPU 1000000UL |   1.0  MHz |    0011    |    8 -> �C default
    //    #define F_CPU  500000UL | 500.0  kHz |    0100    |   16
    //    #define F_CPU  250000UL | 250.0  kHz |    0101    |   32
    //    #define F_CPU  125000UL | 125.0  kHz |    0110    |   64
    //    #define F_CPU   62500UL |  62.5  kHz |    0111    |  128
    //    #define F_CPU   31250UL |  31.25 kHz |    1000    |  256
    //////////////////////////////////////////////////////////////////////////
    
    cli();                  // disable interrupts
    uint8_t tCLKPR = 0;     // CLKPCE is cleared by hardware four cycles after it's written
    switch(f) {                // configure prescaler
        case    f8M: tCLKPR = (0<<CLKPS3)|(0<<CLKPS2)|(0<<CLKPS1)|(0<<CLKPS0); break;
        case    f4M: tCLKPR =                                     (1<<CLKPS0); break;
        case    f2M: tCLKPR =                         (1<<CLKPS1)            ; break;
        case    f1M: tCLKPR =                         (1<<CLKPS1)|(1<<CLKPS0); break;
        case  f500k: tCLKPR =             (1<<CLKPS2)                        ; break;
        case  f250k: tCLKPR =             (1<<CLKPS2)|            (1<<CLKPS0); break;
        case  f125k: tCLKPR =             (1<<CLKPS2)|(1<<CLKPS1)            ; break;
        case  f62k5: tCLKPR =             (1<<CLKPS2)|(1<<CLKPS1)|(1<<CLKPS0); break;
        case f31k25: tCLKPR = (1<<CLKPS3)|(0<<CLKPS2)|(0<<CLKPS1)|(0<<CLKPS0); 
    }
    CLKPR = (1<<CLKPCE);    // clock prescaler change enable
    CLKPR = tCLKPR;         // write clock prescale configurations
    sei();                  // global interrupt enable
}


