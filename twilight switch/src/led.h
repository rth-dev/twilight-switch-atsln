/************************************************************************
 * @file       led.h
 * @autor      rth.
 * @date       27 Mar 2018
 * @brief      LED implementation
 *
 * @license    GNU GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 *
 ************************************************************************/


#ifndef LED_H_
#define LED_H_

#include "system.h"

#include <stdint.h>
#include <stdbool.h>

void ledSwitch(uint8_t pin, bool state);



#endif /* LED_H_ */
