/************************************************************************
 * @file       system.h
 * @autor      rth.
 * @date       27 Mar 2018
 * @brief      system configuration
 *
 * @license    GNU GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 *
 ************************************************************************/


#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdint.h>
#include <stdbool.h>

#define LED1    PB0
#define LED2    PB1
#define LEDPORT PORTB

enum cpuFreq {f8M, f4M, f2M, f1M, f500k, f250k, f125k, f62k5, f31k25};

void init(enum cpuFreq f, bool wdgEna);
void setClockPrescaler(enum cpuFreq f);

#endif /* SYSTEM_H_ */