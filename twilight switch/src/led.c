/************************************************************************
 * @file       led.c
 * @autor      rth.
 * @date       27 Mar 2018
 * @brief      LED implementation
 *
 * @license    GNU GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
 *
 ************************************************************************/

#include "led.h"
#include "system.h"

#include <avr/io.h>



/*____________________________________________________________________________*/
void ledSwitch(uint8_t pin, bool state) {

    switch(pin) {
        case LED1:  if(state) OCR0A = 0xFF;
                    else      OCR0A = 0x00;
                    break;
        case LED2:  if(state) OCR0B = 0xFF;
                    else      OCR0B = 0x00;
                    break;
    }
}


